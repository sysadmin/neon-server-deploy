# Deploy Aptly on a server

Run ./deploy to setup Aptly on a server for KDE neon. It provides the
package archive that Jenkins at build.neon.kde.org uploads packages
to.

Uses ansible to deploy, an IaC devops management tool.
